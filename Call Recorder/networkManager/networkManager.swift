//
//  networkManager.swift
//  Call Recorder
//
//  Created by Developer on 06/01/22.
//


import Foundation
import UIKit
import Alamofire
import SwiftyJSON

let BaseURL = "http://abhirecorder.com/aminiCallRecorder/api/"
let imageURL = "http://abhirecorder.com/aminiCallRecorder/assets/images/contact/"

struct APIS {
    static let LoginAuth = BaseURL + "user-login-register"
    static let OTPVirify = BaseURL + "otp-verify"
    static let AuthUser =  BaseURL + "auth-user"
    static let resentOTP = BaseURL + "resend-otp"
    static let historyList = BaseURL + "recording-list"
    static let AssistNumbers = BaseURL + "contact-number-list"
    static let CheckPassword = BaseURL + "check_recording_password"
    static let updatePassword = BaseURL + "update_recording_password"
    static let deleteCall = BaseURL  + "delete_recording"
    static let changePassword = BaseURL + "change-password"
}

struct Parameterkeys {
    static let id = "id"
    static let contactNumber = "contact_number"
    static let otp = "otp"
    static let isVerify = "isVerify"
    static let userID = "user_id"
    static let password = "password"
}


struct userLoginKeys {
    static let status = "contact_number"
    static let message = "otp"
    static let user_id = "user_id"
    static let user_name = "user_name"
    static let user_email = "user_email"
    static let contact_number = "contact_number"
    static let is_protected = "is_protected"
    static let contact_verified = "contact_verified"
    
}

class NetworkManager {
    func getDataFromServer(url:String,method:HTTPMethod,parameter:Parameters,view:UIView,successBlock:@escaping (_ response: JSON )->Void) {
        showActivityWithMessage(message: "", inView: view)
        print("parameter \(parameter)")
        print("url \(url)")
        AF.request(url, method: method, parameters: parameter, headers: nil).responseJSON { (response ) in
            hideActivity()
            
            switch response.result {
            case .success:
                successBlock(JSON(response.data!))
            case .failure(let error):
                print(error)
            }
        }
    }
    
}
