//
//  constant.swift
//  Call Recorder
//
//  Created by Developer on 06/01/22.
//


import Foundation
import UIKit
import SwiftyJSON
import Contacts

struct AppColors {
    static var AppBlackcolor:UIColor = .black
    static var AppGraycolor:UIColor =  #colorLiteral(red: 0.1019607843, green: 0.1019607843, blue: 0.1019607843, alpha: 1)
    static var AppPurpalColor:UIColor = #colorLiteral(red: 0.568627451, green: 0.2235294118, blue: 0.9803921569, alpha: 1)
    static var AppYellowColor:UIColor = #colorLiteral(red: 0.9803921569, green: 0.7764705882, blue: 0.2235294118, alpha: 1)
    
}


struct  AppFlyerKeys {
    let devKey = "pixwSK68ZSrjHiPfrVoADK"
    
    }

let appid = "1607799989"


extension NSNotification.Name{
    static let purchaseNotif = Notification.Name("purchaseNotife")
    static let noRestordData = Notification.Name("noRestordData")
}

let appName = "Record calls- call recorder"

var contrySy = ""



var wPrice = ""
var mPrice = ""
var yPrice = ""

var fyPrice = ""
var fMPrice = ""
var fWPrice = ""


var mOfferPrice = ""
var wOfferPrice = ""
var yOfferPrice = ""




var userDataLoged = JSON()
var countryListData = [JSON]()
// ACTIVIY INDICATOR

var blackView: UIView? = nil
func showActivityWithMessage(message: String, inView view: UIView) {
    if blackView == nil {
        blackView = UIView(frame: UIScreen.main.bounds)
        blackView!.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        var factor: Float = 1.0
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
            factor = 2.0
        }
        
        var rect = CGRect.init()
        rect.size.width = CGFloat(factor * Float(200))
        rect.size.height = CGFloat(factor * Float(70))
        rect.origin.y = CGFloat((blackView?.frame.size.height)! - rect.size.height) / CGFloat(2.0)
        rect.origin.x = CGFloat((blackView?.frame.size.width)! - rect.size.width) / CGFloat(2.0)
        
        let waitingView = UIView(frame: rect)
        waitingView.backgroundColor = UIColor.clear
        waitingView.layer.cornerRadius = 8.0
        
        rect = waitingView.bounds;
        rect.size.height = CGFloat(Float(40.0) * factor);
        
        let label = UILabel(frame: rect)
        label.text = message
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: CGFloat(Float(16.0) * factor))//UIFont(name: APP_BOLD_FONT, size: CGFloat(Float(16.0) * factor))
        waitingView.addSubview(label)
        var activityIndicatorView = UIActivityIndicatorView()
        if #available(iOS 13.0, *) {
            activityIndicatorView = UIActivityIndicatorView.init(style: UIActivityIndicatorView.Style.large)
        } else {
            activityIndicatorView = UIActivityIndicatorView.init(style: UIActivityIndicatorView.Style.gray)
        }
        activityIndicatorView.color = UIColor.white
        
        rect = activityIndicatorView.frame;
        rect.origin.x = ((waitingView.frame.size.width - rect.size.width)/2.0);
        rect.origin.y = label.frame.size.height + 3.0;
        activityIndicatorView.frame = rect;
        
        waitingView.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()
        
        blackView?.tag = 1010
        blackView?.addSubview(waitingView)
        view.addSubview(blackView!)
    }
}

func showAlet(message:String,controller:UIViewController) {
    let alertMessage = UIAlertAction(title: "Ok", style: .default) { (action) in
        
    }
    
    let alert = UIAlertController(title: "Warnning!", message: "\(message)", preferredStyle: .alert)
    alert.addAction(alertMessage)
    controller.present(alert, animated: true, completion: nil)
}


func hideActivity() {
    blackView?.removeFromSuperview()
    blackView = nil
}

extension UIViewController {

func showToast(message : String, font: UIFont) {

    let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 150, height: 35))
    toastLabel.backgroundColor = AppColors.AppPurpalColor
    toastLabel.textColor = UIColor.white
    toastLabel.font = font
    toastLabel.textAlignment = .center;
    toastLabel.text = message
    toastLabel.alpha = 1.0
    toastLabel.layer.cornerRadius = 10;
    toastLabel.clipsToBounds  =  true
    self.view.addSubview(toastLabel)
    UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
         toastLabel.alpha = 0.0
    }, completion: {(isCompleted) in
        toastLabel.removeFromSuperview()
    })
} }

func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
  return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
}

func convertDateTimeFormate(dateformate:String,convertedFormate:String,date:String) -> String {
    let dateformater = DateFormatter()
    dateformater.dateFormat = dateformate
    let date = dateformater.date(from: date)
    dateformater.dateFormat = convertedFormate
    
    let convertedDate = dateformater.string(from: date ?? Date())
    
    return convertedDate
}
