//
//  UserdefaultManager.swift
//  myvpn
//
//  Created by RMV on 05/03/21.
//  Copyright © 2021 TechnoWaves. All rights reserved.
//

import Foundation
import SwiftyJSON
class UserDefaultsManager {
    
    var purchaseStatus = UserDefaults.standard.value(forKey: userdefault().purchaseStatus) as? Bool {
        didSet { UserDefaults.standard.set(self.purchaseStatus ?? "", forKey: userdefault().purchaseStatus) }
    }
    
    var purchasePlan = UserDefaults.standard.value(forKey: userdefault().purchasePlan) as? String {
        didSet { UserDefaults.standard.set(self.purchasePlan ?? "", forKey: userdefault().purchasePlan) }
    }
    
    
    var isFirstTime = UserDefaults.standard.value(forKey: userdefault().isFirstTime) as? Bool {
        didSet { UserDefaults.standard.set(self.isFirstTime ?? "", forKey: userdefault().isFirstTime) }
    }
    
    var weekPrice = UserDefaults.standard.value(forKey: userdefault().weekPrice) as? String {
        didSet { UserDefaults.standard.set(self.weekPrice ?? "", forKey: userdefault().weekPrice) }
    }
    
    var monthPrice = UserDefaults.standard.value(forKey: userdefault().monthPrice) as? String {
        didSet { UserDefaults.standard.set(self.monthPrice ?? "", forKey: userdefault().monthPrice) }
    }
    
    var yearPrice = UserDefaults.standard.value(forKey: userdefault().yearPrice) as? String {
        didSet { UserDefaults.standard.set(self.yearPrice ?? "", forKey: userdefault().yearPrice) }
    }
    
    
    var selectedCountry = UserDefaults.standard.value(forKey: userdefault().selectedCountry) as? Int {
        didSet { UserDefaults.standard.set(self.selectedCountry ?? "", forKey: userdefault().selectedCountry) }
    }
    var isUserVerifed = UserDefaults.standard.value(forKey: userdefault().isUserVerifed) as? Bool {
        didSet { UserDefaults.standard.set(self.isUserVerifed ?? "", forKey: userdefault().isUserVerifed) }
    }
    
    var userData = UserDefaults.standard.value(forKey: userdefault().userData) as? Data {
        didSet { UserDefaults.standard.set(self.userData ?? "", forKey: userdefault().userData) }
    }

}
struct userdefault {
    let purchasePlan = "purchasePlan"
    let purchaseStatus = "purchaseStatus"
    let isFirstTime = "ShowBannerFirstTime"
    
    let userData = "userLoginData"
    
    let weekPrice = "weeklyPrice"
    let monthPrice = "monthlyPrice"
    let yearPrice = "yearlyPrice"
    
    var isUserVerifed = "userLogedIn"
    
    var selectedCountry = "country"
}

