//
//  AppDelegate.swift
//  Call Recorder
//
//  Created by Developer on 06/01/22.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import Firebase
import AppsFlyerLib

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    static let sharedAppDelegate = AppDelegate()
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IPASubScriptionService.shared.getProduct()
        IPASubScriptionService.shared.validateReceipt()
        FirebaseApp.configure()
        Auth.auth().settings?.isAppVerificationDisabledForTesting = true

        configerAppsFlyerSDK()

        IQKeyboardManager.shared.enable = true
       
        goToRootView()
        return true
    }
    
    func configerAppsFlyerSDK(){
        AppsFlyerLib.shared().appsFlyerDevKey = AppFlyerKeys().devKey
        AppsFlyerLib.shared().appleAppID = appid
        AppsFlyerLib.shared().delegate = self
        AppsFlyerLib.shared().isDebug = false
        AppsFlyerLib.shared().deepLinkDelegate = .none
        AppsFlyerLib.shared().start()
        
    }
    
    func goToRootView() {
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var homecontroller = UIViewController()
        
        
        if !(UserDefaultsManager().isFirstTime ?? false) || UserDefaultsManager().isFirstTime == nil {
            homecontroller = storyboard.instantiateViewController(withIdentifier: "OnBoardingVC") as! OnBoardingVC
        } else if !(UserDefaultsManager().isUserVerifed ?? false){
            homecontroller = storyboard.instantiateViewController(withIdentifier: "SignUpSignInVC") as! SignUpSignInVC
        } else {
            homecontroller = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
        }
        
        let homeNavi = UINavigationController(rootViewController: homecontroller)
        homeNavi.navigationBar.isHidden = true
        homeNavi.interactivePopGestureRecognizer?.isEnabled = false
        self.window?.rootViewController = homeNavi
        self.window?.makeKeyAndVisible()
        
    }
    // Open Deeplinks
    // Open URI-scheme for iOS 8 and below
    private func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        AppsFlyerLib.shared().continue(userActivity, restorationHandler: restorationHandler)
        return true
    }
    // Open URI-scheme for iOS 9 and above
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        AppsFlyerLib.shared().handleOpen(url, sourceApplication: sourceApplication, withAnnotation: annotation)
        return true
    }
    // Report Push Notification attribution data for re-engagements
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        AppsFlyerLib.shared().handleOpen(url, options: options)
        return true
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        AppsFlyerLib.shared().handlePushNotification(userInfo)
    }
    // Reports app open from deep link for iOS 10 or later
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        AppsFlyerLib.shared().continue(userActivity, restorationHandler: nil)
        return true
    }
    
}


extension AppDelegate:AppsFlyerLibDelegate {
    func onConversionDataSuccess(_ conversionInfo: [AnyHashable : Any]) {
        print("onConversionDataSuccess data:")
        for (key, value) in conversionInfo {
            print(key, ":", value)
        }
        if let status = conversionInfo["af_status"] as? String {
            if (status == "Non-organic") {
                if let sourceID = conversionInfo["media_source"],
                   let campaign = conversionInfo["campaign"] {
                    print("This is a Non-Organic install. Media source: \(sourceID)  Campaign: \(campaign)")
                }
            } else {
                print("This is an organic install.")
            }
            if let is_first_launch = conversionInfo["is_first_launch"] as? Bool,
               is_first_launch {
                print("First Launch")
            } else {
                print("Not First Launch")
            }
        }
    }
    
    func onConversionDataFail(_ error: Error) {
        print(error)
    }
    //Handle Deep Link
    func onAppOpenAttribution(_ attributionData: [AnyHashable : Any]) {
        //Handle Deep Link Data
        print("onAppOpenAttribution data:")
        for (key, value) in attributionData {
            print(key, ":",value)
        }
    }
    func onAppOpenAttributionFailure(_ error: Error) {
        print(error)
    }
    
    
}
