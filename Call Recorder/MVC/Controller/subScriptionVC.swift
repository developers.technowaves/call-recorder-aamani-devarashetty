//
//  subScriptionVC.swift
//  Call Recorder
//
//  Created by Developer on 07/01/22.
//

import UIKit

class subScriptionVC: UIViewController {

    @IBOutlet weak var subScriptionBtn: UIButton!
    @IBOutlet weak var yearOptionImg: UIImageView!
    @IBOutlet weak var monthOptionImg: UIImageView!
    @IBOutlet weak var weekOptionImg: UIImageView!
    
    @IBOutlet weak var restoreBt: UIButton!
    
    @IBOutlet weak var yearPriceLbl: UILabel!
    @IBOutlet weak var monthPriceLbl: UILabel!
    @IBOutlet weak var weeklyPriceLbl: UILabel!
    
    
    
    @IBOutlet weak var yearView: UIView!
    @IBOutlet weak var monthView: UIView!
    @IBOutlet weak var weekView: UIView!
    
    
    
    var isWhereToCome = ""

    var selectedIndex = 0
    
    @IBOutlet weak var closeBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        if isWhereToCome == "pop" {
            restoreBt.isHidden = true
            closeBtn.setImage(#imageLiteral(resourceName: "close"), for: .normal)

        } else {
            if UserDefaultsManager().purchaseStatus == true {
                restoreBt.isHidden = true
            } else {
                restoreBt.isHidden = false
            }
            
            closeBtn.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        }
        
        
        if UserDefaultsManager().weekPrice == "" && UserDefaultsManager().monthPrice == "" && UserDefaultsManager().yearPrice == ""  {
            DispatchQueue.main.async {
                IPASubScriptionService.shared.getProduct()
                self.weeklyPriceLbl.text = "\(UserDefaultsManager().weekPrice ?? "") / Week"
                self.monthPriceLbl .text = "\(UserDefaultsManager().monthPrice ?? "") / Month"
                self.yearPriceLbl.text = "\(UserDefaultsManager().yearPrice ?? "") /  Year"


            }

        } else {
            weeklyPriceLbl.text = "\(UserDefaultsManager().weekPrice ?? "") / Week"
            monthPriceLbl.text = "\(UserDefaultsManager().monthPrice ?? "") / Month"
            yearPriceLbl.text = "\(UserDefaultsManager().yearPrice ?? "") /  Year"



        }
        if UserDefaultsManager().purchasePlan == IPAProduct.Mounth.rawValue {
            selectionOption(selecte: 2)
        } else if UserDefaultsManager().purchasePlan == IPAProduct.Yearly.rawValue {
            selectionOption(selecte: 1)
        } else if UserDefaultsManager().purchasePlan == IPAProduct.Week.rawValue {
            selectionOption(selecte: 3)
        } else {
            selectionOption(selecte: 1)
        }
        
        _ = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(subscrptionBtnAnimation), userInfo: nil, repeats: true)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.checkSubscriptionOrNot), name: .purchaseNotif, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.noRestoreData), name: .noRestordData, object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func restoreBtnTap(_ sender: UIButton) {
        showActivityWithMessage(message: "", inView: self.view)
        IPASubScriptionService.shared.restorePurchases()
        
    }
    @IBAction func backBtnTap(_ sender: UIButton) {
        if isWhereToCome == "pop" {
            
            self.dismiss(animated: true, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    @IBAction func YearOptionBtnTap(_ sender: UIButton) {
        selectionOption(selecte: 1)
    }
    @IBAction func monthOptionBtnTap(_ sender: UIButton) {
        selectionOption(selecte: 2)
    }
    @IBAction func weekOptionBtnTap(_ sender: UIButton) {
        selectionOption(selecte: 3)
    }
    
    @IBAction func continueBtnTap(_ sender: UIButton) {
        
        purchesPlan()

    }
    func purchesPlan() {
        if selectedIndex == -1 {
            
            showAlet(message: "Please select subscription plan", controller: self)
        } else {
            switch selectedIndex {
            case 1:
                //week
                showActivityWithMessage(message: "Loading...", inView: self.view)
                IPASubScriptionService.shared.purchase(product: .Yearly)
                break
            case 2:
                //mounth
                showActivityWithMessage(message: "Loading...", inView: self.view)
                IPASubScriptionService.shared.purchase(product: .Mounth)
                break
            case 3:
                //mounth
                showActivityWithMessage(message: "Loading...", inView: self.view)
                IPASubScriptionService.shared.purchase(product: .Week)
                break
                
            default:
                showAlet(message: "Please select subscription plan", controller: self)
            }
        }
        
    }
    func selectionOption(selecte:Int) {
        selectedIndex = selecte
        switch selectedIndex {
        case 1:
            
            
            yearOptionImg.image = #imageLiteral(resourceName: "Check")
            monthOptionImg.image = #imageLiteral(resourceName: "unCheck")
            weekOptionImg.image = #imageLiteral(resourceName: "unCheck")
            
            yearView.backgroundColor = AppColors.AppPurpalColor
            monthView.backgroundColor = .black
            weekView.backgroundColor = .black
            break
        case 2:
            
            monthOptionImg.image = #imageLiteral(resourceName: "Check")
            yearOptionImg.image = #imageLiteral(resourceName: "unCheck")
            weekOptionImg.image = #imageLiteral(resourceName: "unCheck")
            
            
            yearView.backgroundColor = .black
            monthView.backgroundColor = AppColors.AppPurpalColor
            weekView.backgroundColor = .black
            break
        case 3:
            
            weekOptionImg.image = #imageLiteral(resourceName: "Check")
            yearOptionImg.image = #imageLiteral(resourceName: "unCheck")
            monthOptionImg.image = #imageLiteral(resourceName: "unCheck")
            
            
            yearView.backgroundColor = .black
            monthView.backgroundColor = .black
            weekView.backgroundColor = AppColors.AppPurpalColor
            break
        default:
            break
        }
    }
    
    @objc func noRestoreData(){
        let alert = UIAlertController(title: "Subscription Information", message: "you are not Subscribe any plan right now", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alert.addAction(ok)
        present(alert, animated: true, completion: nil)
    }
    
    @objc  func subscrptionBtnAnimation() {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: [.allowUserInteraction], animations: {
            self.subScriptionBtn.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)
        }) { (_) in
            UIView.animate(withDuration: 0.5, delay: 0.0, options: [.allowUserInteraction], animations: {
                self.subScriptionBtn.transform = CGAffineTransform.identity
            }, completion: nil)
        }
    }
    
    @objc func checkSubscriptionOrNot(){
        if UserDefaultsManager().purchaseStatus ?? false {
            UserDefaultsManager().purchaseStatus = true
            AppDelegate.sharedAppDelegate.goToRootView()
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
