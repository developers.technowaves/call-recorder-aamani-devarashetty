//
//  SettingVC.swift
//  Call Recorder
//
//  Created by Developer on 18/01/22.
//

import UIKit

class SettingVC: UIViewController {

    @IBOutlet weak var callrecorderToggle: UISwitch!
    @IBOutlet weak var notificationToggle: UISwitch!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func callrecorderswitch(_ sender: UISwitch) {
    }
    @IBAction func notificationSwitch(_ sender: UISwitch) {
    }
    @IBAction func subscriptionBtnTap(_ sender: UIButton) {
        let subscriptionvc = self.storyboard?.instantiateViewController(withIdentifier: "subScriptionVC") as! subScriptionVC
        self.navigationController?.pushViewController(subscriptionvc, animated: true)
    }
    @IBAction func menuBtnTap(_ sender: UIButton) {
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
