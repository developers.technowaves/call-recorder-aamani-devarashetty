//
//  TabBatControllerVC.swift
//  Call Recorder
//
//  Created by Developer on 18/01/22.
//

import UIKit
import SOTabBar
class TabBatControllerVC: SOTabBarController, SOTabBarControllerDelegate {
    override func loadView() {
        super.loadView()
        SOTabBarSetting.tabBarTintColor = #colorLiteral(red: 0.568627451, green: 0.2235294118, blue: 0.9803921569, alpha: 1)
        SOTabBarSetting.tabBarHeight = 50
        SOTabBarSetting.tabBarBackground = #colorLiteral(red: 0.1294117647, green: 0.1333333333, blue: 0.1607843137, alpha: 1)
        SOTabBarSetting.tabBarShadowColor = UIColor.clear.cgColor
        SOTabBarSetting.tabBarCircleSize = CGSize(width: 65, height: 65)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        let homeStoryboard = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeVC")
        let contactList = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ContactListVC")
        let settingvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileVC")
        
        homeStoryboard.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "phone-s"), selectedImage: UIImage(named: "phone-s"))
        
        
        contactList.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "users-s"), selectedImage: UIImage(named: "users-s"))
        
        settingvc.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "sliders-s"), selectedImage: UIImage(named: "sliders-s"))
        
        viewControllers = [homeStoryboard, contactList,settingvc]
        let initialVC = self.storyboard?.instantiateViewController(withIdentifier: "subScriptionVC") as! subScriptionVC
        initialVC.isWhereToCome = "pop"
        print("UserDefaultsManager().purchaseStatus \(UserDefaultsManager().purchaseStatus)")
        if UserDefaultsManager().purchaseStatus == false || UserDefaultsManager().purchaseStatus == nil {
            
                self.present(initialVC, animated: true) {
                    print("done ")
                }
        }
        // Do any additional setup after loading the view.
    }
    func tabBarController(_ tabBarController: SOTabBarController, didSelect viewController: UIViewController) {
        print(viewController.tabBarItem.title ?? "")
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
