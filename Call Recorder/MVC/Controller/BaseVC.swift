//
//  BaseVC.swift
//  Call Recorder
//
//  Created by Developer on 19/01/22.
//

import UIKit
import SideMenu
class BaseVC: UIViewController,SWRevealViewControllerDelegate {

    
    func revealController(_ revealController: SWRevealViewController!, willMoveTo position: FrontViewPosition) {
        print("position \(position)")
        
        if position.rawValue == 3 {
            self.view.removeGestureRecognizer(tapSideMenu)
        }
    }
    var sideMenuPan = UIPanGestureRecognizer()
    var tapSideMenu = UITapGestureRecognizer()
    override func viewDidLoad() {
        super.viewDidLoad()
        
                self.revealViewController()?.delegate = self
        self.revealViewController()?.rearViewRevealWidth = UIScreen.main.bounds.size.width - 70
                createPanGestureRecognizer()
                createTapGestureRecognizer()
        // Do any additional setup after loading the view.
    }
    
    
    func createTapGestureRecognizer() {
        tapSideMenu = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture))

    }
    func createPanGestureRecognizer() {
        sideMenuPan = UIPanGestureRecognizer(target: self, action:#selector(handlePanGesture))
        self.view.addGestureRecognizer(sideMenuPan)
    }
    
    @objc func handleTapGesture(tap:UITapGestureRecognizer) {
        self.revealViewController()?.rightRevealToggle(self)

    }
    
    @objc func handlePanGesture(panGesture: UIPanGestureRecognizer) {
        if panGesture.state == .ended {
            
            if self.revealViewController()?.frontViewPosition.rawValue == 3 {
                self.revealViewController()?.revealToggle(self)
            } else  {
                self.revealViewController()?.rightRevealToggle(self)
                
            }
        }
        
        

    }
    func subscriptionVC() {
        let subscriptionvc = self.storyboard?.instantiateViewController(withIdentifier: "subScriptionVC") as! subScriptionVC
        self.navigationController?.pushViewController(subscriptionvc, animated: true)
    }
    func showSideMenu() {
        self.view.addGestureRecognizer(tapSideMenu)

        self.revealViewController()?.rearViewRevealWidth = view.frame.width / 1.3
        self.revealViewController().revealToggle(self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
