//
//  HomeVC.swift
//  Call Recorder
//
//  Created by Developer on 10/01/22.
//

import UIKit
import BBWebImage
import Alamofire
import SwiftyJSON
import Contacts

class HomeVC: BaseVC {

    var historyList = [JSON]()
    lazy var contacts: [CNContact] = {
        let contactStore = CNContactStore()
        let keysToFetch = [
            CNContactFamilyNameKey,
            CNContactMiddleNameKey,
            CNContactGivenNameKey,
            CNContactEmailAddressesKey,
            CNContactPhoneNumbersKey,
            CNContactImageDataAvailableKey,
            CNContactThumbnailImageDataKey] as [CNKeyDescriptor]
        
        // Get all the containers
        var allContainers: [CNContainer] = []
        do {
            allContainers = try contactStore.containers(matching: nil)
        } catch {
            print("Error fetching containers")
        }
        
        var results: [CNContact] = []
        
        // Iterate all containers and append their contacts to our results array
        for container in allContainers {
            let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
            
            do {
                let containerResults = try contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch)
                results.append(contentsOf: containerResults)
            } catch {
                print("Error fetching results for container")
            }
        }
        
        return results
    }()

    @IBOutlet weak var historyTbl: UITableView!
    let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.historyTbl.isHidden = true
//        userDataLoged = JSON(UserDefaultsManager().userData!)

        historyTbl.delegate = self
        historyTbl.dataSource = self
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        refreshControl.tintColor = .white

        historyTbl.addSubview(refreshControl) // not required when using UITableViewController

        historyData()
        //Do any additional setup after loading the view.
    }
    @objc func refresh(_ sender: AnyObject) {
       // Code to refresh table view
        refreshControl.beginRefreshing()
        historyData()
    }
    func historyData() {
        let parameter:Parameters = ["\(Parameterkeys.userID)":"\(userDataLoged["\(userLoginKeys.user_id)"].intValue)"]
        
//        let parameter:Parameters = ["\(Parameterkeys.userID)":"36"]
              
        NetworkManager().getDataFromServer(url: APIS.historyList, method: .post, parameter: parameter, view: self.view) { (json) in
            self.refreshControl.endRefreshing()
            if json["status"].boolValue == true {
                self.historyList = json["user_recording_list"].arrayValue
                if self.historyList.count > 0 {
                    self.historyTbl.isHidden = false
                    
                    self.historyTbl.reloadData()
                } else {
                    self.historyTbl.isHidden = true
                }
                
            } else {
                showAlet(message: "\(json["message"].stringValue)", controller: self)
            }
        }
    }
    
    @IBAction func menuBtnTap(_ sender: UIButton) {
       
        showSideMenu()
    }
    @IBAction func subscriptionBtnTap(_ sender: UIButton) {
        let subscriptionvc = self.storyboard?.instantiateViewController(withIdentifier: "subScriptionVC") as! subScriptionVC
        self.navigationController?.pushViewController(subscriptionvc, animated: true)
    }
    
    @IBAction func CallplayBtnTap(_ sender: UIButton) {
//        kTestLink = "http://traffic.libsyn.com/innovationengine/Disruptive_Innovation_in_Media__Entertainment.mp3"
        kTestLink = historyList[sender.tag]["recording_url"].stringValue

        let playerVc = self.storyboard?.instantiateViewController(withIdentifier: "AudioPlayerVC") as!
            AudioPlayerVC
        self.present(playerVc, animated: true, completion: nil)
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension HomeVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyList.count
//        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "historyTblCell", for: indexPath) as! historyTblCell
        let contact = searchForContactUsingPhoneNumber(phoneNumber: "\(self.historyList[indexPath.row]["from"].stringValue)")
        
        if contact.count > 0 {
            cell.userImg.image = UIImage(data: contact.first?.thumbnailImageData ?? Data()) ?? #imageLiteral(resourceName: "2")

            cell.usernameLbl.text = "\(contact.first?.givenName ?? self.historyList[indexPath.row]["from"].stringValue) \(contact.first?.familyName ?? "")"
        } else {
            
            cell.usernameLbl.text = self.historyList[indexPath.row]["from"].stringValue
        }
    
        cell.callTimeLbl.text = convertDateTimeFormate(dateformate: "yyyy-mm-ddThh:mm:ss", convertedFormate: "dd-mm-yyyy", date: self.historyList[indexPath.row]["created_at"].stringValue)
        
        cell.callDateLbl.text = convertDateTimeFormate(dateformate: "yyyy-mm-ddThh:mm:ss", convertedFormate: "hh:mm a", date: self.historyList[indexPath.row]["created_at"].stringValue)
        
        
        return cell
    }
    
    
}

class historyTblCell:UITableViewCell {
    
    @IBOutlet weak var callRecorderPlayBtn: UIButton!
    @IBOutlet weak var callDateLbl: UILabel!
    @IBOutlet weak var callTimeLbl: UILabel!
    @IBOutlet weak var callTypeImg: UIImageView!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var usernameLbl: UILabel!
    override func awakeFromNib() {
        callTypeImg.isHidden = true
    }
}
extension HomeVC {
    func searchForContactUsingPhoneNumber(phoneNumber: String) -> [CNContact] {
        
        var result: [CNContact] = []
        
        for contact in self.contacts {
            if (!contact.phoneNumbers.isEmpty) {
                let phoneNumberToCompareAgainst = phoneNumber.components(separatedBy: NSCharacterSet.decimalDigits.inverted).joined(separator: "")
                for phoneNumber in contact.phoneNumbers {
                    if let phoneNumberStruct = phoneNumber.value as? CNPhoneNumber {
                        let phoneNumberString = phoneNumberStruct.stringValue
                        let phoneNumberToCompare = phoneNumberString.components(separatedBy: NSCharacterSet.decimalDigits.inverted).joined(separator: "")
                        print("phoneNumberToCompare \(phoneNumberToCompare)")
                        print("phoneNumberToCompareAgainst \(phoneNumberToCompareAgainst)")
                        if phoneNumberToCompare.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "-", with: "") == phoneNumberToCompareAgainst {
                            result.append(contact)
                        }
                    }
                }
            }
        }
        
        return result
    }
}
