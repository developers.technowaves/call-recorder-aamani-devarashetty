//
//  ProfileVC.swift
//  Call Recorder
//
//  Created by Developer on 19/01/22.
//

import UIKit
import Contacts
import SwiftyJSON
import Alamofire
class ProfileVC: BaseVC {

    @IBOutlet weak var userPhoneNumber: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userProfile: UIImageView!
    lazy var contacts: [CNContact] = {
        let contactStore = CNContactStore()
        let keysToFetch = [
            CNContactFamilyNameKey,
            CNContactMiddleNameKey,
            CNContactGivenNameKey,
            CNContactEmailAddressesKey,
            CNContactPhoneNumbersKey,
            CNContactImageDataAvailableKey,
            CNContactThumbnailImageDataKey] as [CNKeyDescriptor]
        
        // Get all the containers
        var allContainers: [CNContainer] = []
        do {
            allContainers = try contactStore.containers(matching: nil)
        } catch {
            print("Error fetching containers")
        }
        
        var results: [CNContact] = []
        
        // Iterate all containers and append their contacts to our results array
        for container in allContainers {
            let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
            
            do {
                let containerResults = try contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch)
                results.append(contentsOf: containerResults)
            } catch {
                print("Error fetching results for container")
            }
        }
        
        return results
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        print("will appear")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        userDataLoged = JSON(UserDefaultsManager().userData!)
        
        let contact = searchForContactUsingPhoneNumber(phoneNumber: "\(userDataLoged["\(userLoginKeys.contact_number)"].stringValue)")
        print("count numbes \(contact.count)")
        userName.text = "-"
        if contact.count > 0 {
            userName.text = "\(contact.first?.givenName ?? "") \(contact.first?.familyName ?? "")"
            userProfile.image = UIImage(data: contact.first?.thumbnailImageData ?? Data()) ?? #imageLiteral(resourceName: "2")
            userPhoneNumber.text = contact.first?.phoneNumbers.first?.value.stringValue
        } else {
            userPhoneNumber.text = userDataLoged["\(userLoginKeys.contact_number)"].stringValue
            userProfile.image = #imageLiteral(resourceName: "2")
        }

        // Do any additional setup after loading the view.
    }
    
    @IBAction func upgradBtnTap(_ sender: UIButton) {
        subscriptionVC()
    }
    @IBAction func sideMenuBtnTap(_ sender: UIButton) {
        showSideMenu()

    }
    
    @IBAction func subscriptionBtnTap(_ sender: UIButton) {
        subscriptionVC()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ProfileVC {
    func searchForContactUsingPhoneNumber(phoneNumber: String) -> [CNContact] {
        
        var result: [CNContact] = []
        
        for contact in self.contacts {
            if (!contact.phoneNumbers.isEmpty) {
                let phoneNumberToCompareAgainst = phoneNumber.components(separatedBy: NSCharacterSet.decimalDigits.inverted).joined(separator: "")
                for phoneNumber in contact.phoneNumbers {
                    if let phoneNumberStruct = phoneNumber.value as? CNPhoneNumber {
                        let phoneNumberString = phoneNumberStruct.stringValue
                        let phoneNumberToCompare = phoneNumberString.components(separatedBy: NSCharacterSet.decimalDigits.inverted).joined(separator: "")
                        print("phoneNumberToCompare \(phoneNumberToCompare)")
                        print("phoneNumberToCompareAgainst \(phoneNumberToCompareAgainst)")
                        if phoneNumberToCompare.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "-", with: "") == phoneNumberToCompareAgainst {
                            result.append(contact)
                        }
                    }
                }
            }
        }
        
        return result
    }
}
