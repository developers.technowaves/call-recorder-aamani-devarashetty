//
//  howRecordToOutCommingCallVC.swift
//  Call Recorder
//
//  Created by Developer on 24/01/22.
//

import UIKit

class howRecordToOutCommingCallVC: UIViewController {
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var pageController: UIPageControl!
    @IBOutlet weak var infoCollection: UICollectionView!
    var VisibleIndex = 0

//    var ImageArray = [#imageLiteral(resourceName: S1),#imageLiteral(resourceName: S2),#imageLiteral(resourceName: S3),#imageLiteral(resourceName: S4)]
    
    var ImageArray = [UIImage(named: "S1"),UIImage(named: "S2"),UIImage(named: "S3"),UIImage(named: "S4")]
    
//    var Title:[String] = ["How to recoard outgoing calls"]
    var subTitle:[String] = ["Tap on Call button in the application","Tap on Call button in the application","Tap the ‘merge calls’ button to start recoarding the Conversation","Once the connection with the assist number established, tap ‘add call’ to add a contact you want to call"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nextBtn.tag = VisibleIndex
        infoCollection.delegate = self
        infoCollection.dataSource = self
        pageController.numberOfPages = ImageArray.count
        pageController.currentPageIndicatorTintColor = AppColors.AppYellowColor
        pageController.tintColor = AppColors.AppGraycolor
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backBtnTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextBtnTap(_ sender: UIButton) {
        if VisibleIndex == subTitle.count {
            
            
            
            VisibleIndex = 0
            self.infoCollection.contentOffset = CGPoint(x: UIScreen.main.bounds.width, y: 0)
        } else  {
            VisibleIndex += 1
            self.infoCollection.contentOffset = CGPoint(x: Int(UIScreen.main.bounds.width)*VisibleIndex, y: 0)

            
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension howRecordToOutCommingCallVC :UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let index = self.infoCollection.contentOffset.x / self.infoCollection.frame.size.width
        
        self.pageController.currentPage = Int(index)
        
        self.nextBtn.setTitle("Next".uppercased(), for: .normal)
        
//        scrollAutomatically()
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  subTitle.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecorderCall", for: indexPath) as! RecorderCall
        
        cell.titleLbl.text = "How to recoard outgoing calls"
        cell.infoImage.image = ImageArray[indexPath.row]
        cell.subtitleLbl.text = subTitle[indexPath.row]

//        cell.infoSubLbl.text = infoSubString[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height)
    }
    
}

class RecorderCall:UICollectionViewCell {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var subtitleLbl: UILabel!
    @IBOutlet weak var infoImage: UIImageView!
    override func awakeFromNib() {
        
    }
}
