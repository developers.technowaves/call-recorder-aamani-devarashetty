//
//  PrivacyAndTermsConditionVC.swift
//  Call Recorder
//
//  Created by Developer on 20/01/22.
//

import UIKit

class PrivacyAndTermsConditionVC: UIViewController {

    var whereToCome = ""
    var titleString = ""
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var headerTitleLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        headerTitleLbl.text = titleString
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backBtnTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
