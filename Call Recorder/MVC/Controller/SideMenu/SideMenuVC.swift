//
//  SideMenuVC.swift
//  Call Recorder
//
//  Created by Developer on 18/01/22.
//

import UIKit
import SideMenu
import Firebase
import StoreKit
import Contacts
import SwiftyJSON
import MessageUI
class SideMenuVC: UIViewController, MFMailComposeViewControllerDelegate {

    @IBOutlet weak var sideMenuTbl: UITableView!
    @IBOutlet weak var userNumberLbl: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userImg: UIImageView!
    
    lazy var contacts: [CNContact] = {
        let contactStore = CNContactStore()
        let keysToFetch = [
            CNContactFamilyNameKey,
            CNContactMiddleNameKey,
            CNContactGivenNameKey,
            CNContactEmailAddressesKey,
            CNContactPhoneNumbersKey,
            CNContactImageDataAvailableKey,
            CNContactThumbnailImageDataKey] as [CNKeyDescriptor]
        
        // Get all the containers
        var allContainers: [CNContainer] = []
        do {
            allContainers = try contactStore.containers(matching: nil)
        } catch {
            print("Error fetching containers")
        }
        
        var results: [CNContact] = []
        
        // Iterate all containers and append their contacts to our results array
        for container in allContainers {
            let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
            
            do {
                let containerResults = try contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch)
                results.append(contentsOf: containerResults)
            } catch {
                print("Error fetching results for container")
            }
        }
        
        return results
    }()
    
    var sideMenuTitle = ["Get Premium","Contact Us","Invite Friends","How to recoard incoming calls?","How to recoard Outgoing calls?","Privacy Policy","Terms And Condition","Rate App","Logout"]
    
    override func viewWillAppear(_ animated: Bool) {
        guard let menu = navigationController as? SideMenuNavigationController, menu.blurEffectStyle == nil else {
            return
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        sideMenuTbl.delegate = self
        sideMenuTbl.dataSource = self
        self.revealViewController()?.rearViewRevealWidth = UIScreen.main.bounds.size.width - 70
        
        userDataLoged = JSON(UserDefaultsManager().userData!)
        
        let contact = searchForContactUsingPhoneNumber(phoneNumber: "\(userDataLoged["\(userLoginKeys.contact_number)"].stringValue)")
        print("count numbes \(contact.count)")
        userName.text = ""
        if contact.count > 0 {
            userName.text = "\(contact.first?.givenName ?? "") \(contact.first?.familyName ?? "")"
            userImg.image = UIImage(data: contact.first?.thumbnailImageData ?? Data()) ?? #imageLiteral(resourceName: "2")
            userNumberLbl.text = contact.first?.phoneNumbers.first?.value.stringValue
        } else {
            userNumberLbl.text = userDataLoged["\(userLoginKeys.contact_number)"].stringValue
            userImg.image = #imageLiteral(resourceName: "2")
        }
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SideMenuVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sideMenuTitle.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sideMenuCell", for: indexPath) as! sideMenuCell
        cell.titleLbl.text = sideMenuTitle[indexPath.row]
        cell.img.image = UIImage(named: "\(indexPath.row + 1)")
        if indexPath.row == 0 {
            cell.titleLbl.textColor = AppColors.AppYellowColor
        } else {
            cell.titleLbl.textColor = .white
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            // permium
            self.revealViewController()?.rightRevealToggle(self)

            let subscriptionvc = self.storyboard?.instantiateViewController(withIdentifier: "subScriptionVC") as! subScriptionVC
            self.navigationController?.pushViewController(subscriptionvc, animated: true)
            break
        case 1:
            // contact us
            let emailTitle = "Support"
            let messageBody = "Feature request Support?"
            let toRecipents = ["Amani.devarashetty@gmail.com"]
            let mc: MFMailComposeViewController = MFMailComposeViewController()
            mc.mailComposeDelegate = self
            mc.setSubject(emailTitle)
            mc.setMessageBody(messageBody, isHTML: false)
            mc.setToRecipients(toRecipents)
            self.present(mc, animated: true, completion: nil)
        
            break
        case 2:
            // invite us
            self.revealViewController()?.rightRevealToggle(self)

            let invitedUs = self.storyboard?.instantiateViewController(withIdentifier: "InviteVC") as! InviteVC
            self.navigationController?.pushViewController(invitedUs, animated: true)
            break
            
        case 3:
            // invite us
            

            let incommingVC = self.storyboard?.instantiateViewController(withIdentifier: "howRecordToInCommingCallVC") as! howRecordToInCommingCallVC
            self.navigationController?.pushViewController(incommingVC, animated: true)
            self.revealViewController()?.rightRevealToggle(self)
            break
           
        case 4:
            // incomming call recorder
            self.revealViewController()?.rightRevealToggle(self)

            let outgoingVC = self.storyboard?.instantiateViewController(withIdentifier: "howRecordToOutCommingCallVC") as! howRecordToOutCommingCallVC
            self.navigationController?.pushViewController(outgoingVC, animated: true)
            break
            
            
        case 5:
            // outgoing call recorder
            self.revealViewController()?.rightRevealToggle(self)
//
//            let prvacy = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyAndTermsConditionVC") as! PrivacyAndTermsConditionVC
//            prvacy.titleString = "Privacy Policy"
//            self.navigationController?.pushViewController(prvacy, animated: true)
            
            
            guard let url = URL(string: "https://sites.google.com/view/amani-privacy-policy/home") ?? URL(string: "https://google.com") else { return  }
            UIApplication.shared.open(url, options: [.universalLinksOnly:false], completionHandler: nil)
            
            break
        case 6:
            // terms and condition
            self.revealViewController()?.rightRevealToggle(self)
//
//            let prvacy = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyAndTermsConditionVC") as! PrivacyAndTermsConditionVC
//            prvacy.titleString = "Terms & Condition"
//            self.navigationController?.pushViewController(prvacy, animated: true)
            
            
            guard let url = URL(string: "https://sites.google.com/view/amani-call-recorder/home") ?? URL(string: "https://google.com") else { return  }
            UIApplication.shared.open(url, options: [.universalLinksOnly:false], completionHandler: nil)
            break
        case 7:
            rateApp()
            // rate app
            break
        case 8:
            let firebaseAuth = Auth.auth()
            do {
              try firebaseAuth.signOut()
                UserDefaultsManager().isUserVerifed = false
                AppDelegate.sharedAppDelegate.goToRootView()

            } catch let signOutError as NSError {
              
            }
            // log out
            break
        default:
            break
        }
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result {
        
        case .cancelled:
            
            print("Mail cancelled")
        case .saved:
            print("Mail saved")
        case .sent:
            print("Mail sent")
        case .failed:
            print("Mail sent failure: \(error?.localizedDescription)")
        default:
            break
        }
        controller.dismiss(animated: true, completion: nil)
    }
}
class sideMenuCell:UITableViewCell {
    
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var img: UIImageView!
    
    override func awakeFromNib() {
        
    }
}
func rateApp() {

    if #available(iOS 10.3, *) {

        SKStoreReviewController.requestReview()
    
    } else {

        let appID = "Your App ID on App Store"
//        let urlStr = "https://itunes.apple.com/app/id\(appID)" // (Option 1) Open App Page
        let urlStr = "https://itunes.apple.com/app/id\(appID)?action=write-review" // (Option 2) Open App Review Page
        
        guard let url = URL(string: urlStr), UIApplication.shared.canOpenURL(url) else { return }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url) // openURL(_:) is deprecated from iOS 10.
        }
    }
}


extension SideMenuVC {
    func searchForContactUsingPhoneNumber(phoneNumber: String) -> [CNContact] {
        
        var result: [CNContact] = []
        
        for contact in self.contacts {
            if (!contact.phoneNumbers.isEmpty) {
                let phoneNumberToCompareAgainst = phoneNumber.components(separatedBy: NSCharacterSet.decimalDigits.inverted).joined(separator: "")
                for phoneNumber in contact.phoneNumbers {
                    if let phoneNumberStruct = phoneNumber.value as? CNPhoneNumber {
                        let phoneNumberString = phoneNumberStruct.stringValue
                        let phoneNumberToCompare = phoneNumberString.components(separatedBy: NSCharacterSet.decimalDigits.inverted).joined(separator: "")
                        print("phoneNumberToCompare \(phoneNumberToCompare)")
                        print("phoneNumberToCompareAgainst \(phoneNumberToCompareAgainst)")
                        if phoneNumberToCompare.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "-", with: "") == phoneNumberToCompareAgainst {
                            result.append(contact)
                        }
                    }
                }
            }
        }
        
        return result
    }
}
