//
//  ContactListVC.swift
//  Call Recorder
//
//  Created by Developer on 18/01/22.
//

import UIKit
import BBWebImage
import Alamofire
import SwiftyJSON
import Contacts
class ContactListVC: BaseVC {

    @IBOutlet weak var contactListTbl: UITableView!
    let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        contactListTbl.delegate = self
        contactListTbl.dataSource = self
        getCountryList()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        refreshControl.tintColor = .white
        
        contactListTbl.addSubview(refreshControl)
        // not required when using UITableViewController
        // Do any additional setup after loading the view.
    }
    @objc func refresh(_ sender: AnyObject) {
       print("getting")
        refreshControl.beginRefreshing()
        getCountryList()
    }
    func getCountryList() {
        
            let parameter = ["":""]
            NetworkManager().getDataFromServer(url: APIS.AssistNumbers, method: .post, parameter: parameter, view: self.view) { (json) in
                self.refreshControl.endRefreshing()

                countryListData.removeAll()
                
                countryListData = json["contact_list"].arrayValue
                self.contactListTbl.reloadData()
                UserDefaultsManager().selectedCountry = 0
               
            }
        
    }
    
    
    
    @IBAction func callBtnTap(_ sender: UIButton) {
        let selectednumber =  countryListData[sender.tag]["contact_number"].stringValue
        print("selectednumber \(selectednumber)")
        
            let pasteboard = UIPasteboard.general
            pasteboard.string = selectednumber
        if UserDefaultsManager().purchaseStatus == true {
            if let url = URL(string: "tel://\(selectednumber)"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        } else {
            let subscriptionvc = self.storyboard?.instantiateViewController(withIdentifier: "subScriptionVC") as! subScriptionVC
            self.navigationController?.pushViewController(subscriptionvc, animated: true)
        }
            
        
    }
    
    @IBAction func subscriptionBtn(_ sender: UIButton) {
        let subscriptionvc = self.storyboard?.instantiateViewController(withIdentifier: "subScriptionVC") as! subScriptionVC
        self.navigationController?.pushViewController(subscriptionvc, animated: true)
    }
    
    @IBAction func menuBtnTap(_ sender: UIButton) {
        showSideMenu()

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ContactListVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countryListData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath) as! contactCell
        cell.userImg.image = nil
        let urlString = "\(imageURL)\(countryListData[indexPath.row]["image"].stringValue )"
        cell.userImg.imageFromUrl(urlString: urlString.replacingOccurrences(of: " ", with: "%20"))
        cell.nameLbl.text = countryListData[indexPath.row]["country"].stringValue
        cell.numberLbl.text = countryListData[indexPath.row]["contact_number"].stringValue
        cell.CallBtn.tag = indexPath.row
        return cell
    }
    
    
}
class contactCell:UITableViewCell {
    
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var CallBtn: UIButton!
    @IBOutlet weak var numberLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    override func awakeFromNib() {
        
    }
}
