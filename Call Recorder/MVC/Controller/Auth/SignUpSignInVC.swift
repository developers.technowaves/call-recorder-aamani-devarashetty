//
//  SignUpSignInVC.swift
//  Call Recorder
//
//  Created by Developer on 07/01/22.
//

import UIKit
import ContactsUI
import Foundation
import Alamofire

import PhoneNumberKit
class SignUpSignInVC: UIViewController, UITextFieldDelegate,CNContactPickerDelegate {

    @IBOutlet weak var mobileNumber: PhoneNumberTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        
        
        // Do any additional setup after loading the view.
    }
    func setupUI(){
        mobileNumber.delegate = self
        mobileNumber.withFlag = true
        mobileNumber.withPrefix = true
        mobileNumber.withExamplePlaceholder = true
        mobileNumber.withDefaultPickerUI = true
        mobileNumber.countryCodePlaceholderColor = .white
        
    }
    
    @IBAction func signInUpBtnTap(_ sender: UIButton) {
//        otpSignInVC
        let otpView = self.storyboard?.instantiateViewController(withIdentifier: "otpSignInVC") as! otpSignInVC
        print("mobileNumber.text \(mobileNumber.text)")
        otpView.mobileNumber = mobileNumber.text ?? ""
        self.navigationController?.pushViewController(otpView, animated: true)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string != "" {
            if mobileNumber.isValidNumber  {
                return false
            } else {
                return true
            }
        } else {
            return true
        }
    
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
