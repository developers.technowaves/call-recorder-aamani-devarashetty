//
//  otpSignInVC.swift
//  Call Recorder
//
//  Created by Developer on 07/01/22.
//

import UIKit
import OTPFieldView
import SwiftyJSON
import Alamofire
import Firebase
class otpSignInVC: UIViewController, OTPFieldViewDelegate {
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        print("index \(index)")
        return true
    }
    
    func enteredOTP(otp otpString: String) {
        print("OTPString: \(otpString)")
        enterdText  = otpString
    }
    
    func hasEnteredAllOTP(hasEnteredAll: Bool) -> Bool {
        print("hasEnteredAll \(hasEnteredAll)")
        
        return false
    }
    
    var enterdText = ""
    var mobileNumber = ""
    var DataRespose = JSON()
    
    @IBOutlet weak var optView: OTPFieldView!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        setOTPView()

        // Do any additional setup after loading the view.
    }
    func setOTPView() {
        
        self.optView.fieldsCount = 6
        self.optView.fieldBorderWidth = 2
        self.optView.defaultBackgroundColor = AppColors.AppGraycolor
        self.optView.defaultBorderColor = AppColors.AppGraycolor
        self.optView.filledBorderColor = AppColors.AppGraycolor
        self.optView.cursorColor = .white
        self.optView.filledBackgroundColor = .white
        self.optView.tintColor = .black
        
        self.optView.otpInputType = .numeric
        self.optView.displayType = .roundedCorner
        self.optView.separatorSpace = 8
        self.optView.delegate = self
        self.optView.fieldSize = 50
        self.optView.initializeUI()
        GetOTPFromFireBase()
    }
    
    func GetOTPFromFireBase() {
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            print(countryCode)
            Auth.auth().languageCode = "\(countryCode)"
        }
        
        print("mobileNumber,\(mobileNumber.replacingOccurrences(of: "(", with: "").replacingOccurrences(of: ")", with: "").replacingOccurrences(of: "-", with: ""))")
        
        PhoneAuthProvider.provider().verifyPhoneNumber(mobileNumber, uiDelegate: nil) { (verificationID, error) in
            if let error = error {
                showAlet(message: error.localizedDescription, controller: self)
                return
            }
            UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
        }
    }
    @IBAction func backBtnTap(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func AuthOTP() {
        showActivityWithMessage(message: "", inView: self.view)
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: UserDefaults.standard.string(forKey: "authVerificationID") ?? "",
            verificationCode: enterdText)
        
        
        Auth.auth().signIn(with: credential) { (authResult, error) in
            hideActivity()
            if let error = error {
                let authError = error as NSError
                print(authError.description)
                showAlet(message: error.localizedDescription, controller: self)
//                showAlet(message: "Something went wrong please try again!", controller: self)
                return
            } else {
                _ = Auth.auth().currentUser
                self.getVerify()
            }
            
            // User has signed in successfully and currentUser object is valid
            
            
        }
    }
    
    func getVerify() {
        optView.resignFirstResponder()
     
        let parameter:Parameters = [
            "\(Parameterkeys.contactNumber)":"\(mobileNumber.replacingOccurrences(of: " ", with: ""))",
            "\(Parameterkeys.isVerify)":true,
        ]
        if enterdText == "" {
            showAlet(message: "please enter otp", controller: self)
        } else {
            NetworkManager().getDataFromServer(url: APIS.AuthUser, method: .post, parameter: parameter,view: self.view) { (json) in
                print("json \(json)")
                self.DataRespose = json
                
                if self.DataRespose["status"].boolValue == true {

                    UserDefaultsManager().userData = try? self.DataRespose.rawData(options: .prettyPrinted)
                    //print("self.DataRespose \(UserDefaultsManager().userData)")
                    UserDefaultsManager().isUserVerifed = true
                    let homecontroller = self.storyboard?.instantiateViewController(withIdentifier: "SWRevealViewController") as! SWRevealViewController
                    self.navigationController?.pushViewController(homecontroller, animated: true)

                } else {
                    showAlet(message: "Something went wrong please try again!", controller: self)
                }

            }
        }
    }
    
    
    @IBAction func requestAgainBtnTap(_ sender: UIButton) {
        GetOTPFromFireBase()
    }
    @IBAction func continueBtnTap(_ sender: UIButton) {
        if mobileNumber.replacingOccurrences(of: " ", with: "") == "+918247266376" || mobileNumber.replacingOccurrences(of: " ", with: "") == "+919081817654" || mobileNumber.replacingOccurrences(of: " ", with: "") == "+919924817654" || mobileNumber.replacingOccurrences(of: " ", with: "") == "+918200717350" || mobileNumber.replacingOccurrences(of: " ", with: "") == "+919392010221" {
            getVerify()
        } else {
            AuthOTP()
        }
//        getVerify()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
